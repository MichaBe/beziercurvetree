# Projekt Béziercurve-Tree (Mitte 2016)

In diesem Projekt interessierten mich die Verwendung der kompletten OpenGL-Grafikpipeline, sowie das rendern von Kurven mittels Tesselation. Das Projekt steckte ich mir selbst, sodass es keine weiteren Randbedingungen gab.

## Verwendete Technologien

* C++
* OpenGL 4.5
* Eigene Grafikbibliothek, die Standardaufgaben vereinfacht

## Kurzbeschreibung

Im ersten Schritt wird die Baumstruktur erstellt. Hierbei nehmen die Anzahl der Äste und die Spannweite nach oben hin zu. An jede so entstandene Strecke wird eine Spirale aus Bézierkurven angelegt. Die dazu benötigten Kontrollpunkte werden in einem Buffer gespeichert, der nach dieser Phase auf der Grafikkarte gespeichert wird. Diese Schritte werden einmal zur Initialisierung ausgeführt. Zur Laufzeit wird das Bild zweimal gerendert.
Das erste Mal werden die Linien gezeichnet. Jeweils vier Bézier-Kontrollpunkte gehören zu einem kubischen Spline. Aus diesen Kontrollpunkten werden in dem Tesselation-Shader die einzelnen Segmente einer Kurve berechnet.
Der zweite Renderpass zeichnet die Dreiecke. Dabei werden die Linien, die der Tesselation-Shader ausgibt, zu randomisierten Dreiecken erweitert, die anschließend zufällig gestreut werden.

## Weiterführende Ressourcen

-