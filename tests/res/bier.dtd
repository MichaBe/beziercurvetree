<!ELEMENT demo (oglconfig?, menu?, sceneLoader,shaderManager,textureManager,objectManager,soundtrackManager,syncManager,cameraManager)>
<!ATTLIST demo
  title CDATA               #REQUIRED>

<!--
1. level beginning here: Elements of 'demo'
-->
<!ELEMENT oglconfig EMPTY>
<!ATTLIST oglconfig
  oglMAJOR CDATA            #REQUIRED
  oglMINOR CDATA            #REQUIRED
  mode (DEBUG)              #IMPLIED>

<!ELEMENT menu (item)*>
<!ATTLIST menu
  fullCustomized (yes)      #IMPLIED>

<!ELEMENT sceneLoader (scene)*>
<!ATTLIST sceneLoader>

<!ELEMENT shaderManager (shader)*>
<!ATTLIST shaderManager
  rootPath CDATA            #REQUIRED>

<!ELEMENT textureManager (texture)*>
<!ATTLIST textureManager
  rootPath CDATA            #REQUIRED>

<!ELEMENT objectManager (object | procedure)*>
<!ATTLIST objectManager
  rootPath CDATA            #REQUIRED>

<!ELEMENT soundtrackManager (sound)*>
<!ATTLIST soundtrackManager
  rootPath CDATA            #REQUIRED
  main     CDATA            #IMPLIED>

<!ELEMENT syncManager (event | trigger)*>

<!ELEMENT cameraManager (frameZero, perspective, keyFrame*)>

<!--
2. level beginning here: Elements of 'menu'
-->
<!ELEMENT item EMPTY>
<!ATTLIST item
  handle CDATA              #IMPLIED
  width CDATA               #REQUIRED
  height CDATA              #REQUIRED>

<!--
2. level: Elements of 'sceneLoader'
-->
<!ELEMENT scene EMPTY>
<!ATTLIST scene
  name CDATA                #REQUIRED
  duration CDATA            #REQUIRED>

<!--
2. level: Elements of 'shaderManager'
-->
<!ELEMENT shader (shdAttribute | shdFeedback)*>
<!ATTLIST shader
  name CDATA                #REQUIRED
  geom (yes)                #IMPLIED
  feedback (interleaved 
  | seperate)               #IMPLIED>

<!--
2. level: Elements of 'textureManager'
-->
<!ELEMENT texture EMPTY>
<!ATTLIST texture
  name CDATA                #REQUIRED
  type (GL_TEXTURE_2D)      #IMPLIED
  file CDATA                #REQUIRED
  WRAP (GL_CLAMP_TO_EDGE 
  | GL_CLAMP_TO_BORDER 
  | GL_REPEAT 
  | GL_MIRRORED_REPEAT 
  | GL_CLAMP)               #IMPLIED
  FILTER (GL_NEAREST 
  | GL_LINEAR)              #IMPLIED
  width CDATA               #REQUIRED
  height CDATA              #REQUIRED
  channels (3 | 4)          #REQUIRED>

<!--
2. level: Elements of 'objectManager'
-->
<!ELEMENT object EMPTY>
<!ATTLIST object
  name CDATA                #REQUIRED
  file CDATA                #REQUIRED
  keep (yes)                #IMPLIED>

<!ELEMENT procedure EMPTY>
<!ATTLIST procedure
  name CDATA                #REQUIRED>

<!--
2. level: Elements of 'soundtrackManager'
-->
<!ELEMENT sound EMPTY>
<!ATTLIST sound
  name CDATA                #REQUIRED
  file CDATA                #REQUIRED>

<!--
2. level: Elements of 'syncManager'
-->
<!ELEMENT event EMPTY>
<!ATTLIST event
  name CDATA                #REQUIRED
  start CDATA               #REQUIRED>

<!ELEMENT trigger EMPTY>
<!ATTLIST trigger
  name CDATA                #REQUIRED
  start CDATA               #REQUIRED
  duration CDATA            #REQUIRED
  repeat CDATA              #REQUIRED>

<!--
2. level: Elements of 'cameraManager'
-->
<!ELEMENT keyFrame EMPTY>
<!ATTLIST keyFrame
  time CDATA                    #REQUIRED
  key ( ROT_X | ROT_Y | ROT_Z
      | TRA_X | TRA_Y | TRA_Z)  #REQUIRED
  value CDATA                   #REQUIRED
  interpolation (SIN | LIN 
  | STEP)                       #REQUIRED>

<!ELEMENT frameZero EMPTY>
<!ATTLIST frameZero
  ROT_X   CDATA             #REQUIRED
  ROT_Y   CDATA             #REQUIRED
  ROT_Z   CDATA             #REQUIRED
  TRA_X   CDATA             #REQUIRED
  TRA_Y   CDATA             #REQUIRED
  TRA_Z   CDATA             #REQUIRED>

<!ELEMENT perspective EMPTY>
<!ATTLIST perspective
  fov     CDATA             #REQUIRED
  near    CDATA             #REQUIRED
  far     CDATA             #REQUIRED>


<!--
3. level beginning here: Elements of 'mb_Loder' and 'shader'
-->
<!ELEMENT shdAttribute EMPTY>
<!ATTLIST shdAttribute
  name CDATA                #REQUIRED>

<!ELEMENT shdFeedback EMPTY>
<!ATTLIST shdFeedback
  name CDATA                #REQUIRED>