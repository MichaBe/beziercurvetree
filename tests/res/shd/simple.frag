#version 450 core

smooth in float fIndexO;

out vec4 color;

void main(void) {
	color = vec4(0.1,0.1,0.7,1.0);
	color.rgb *= vec3(fIndexO);
}