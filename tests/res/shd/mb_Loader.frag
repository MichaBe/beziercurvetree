#version 330

precision highp float;

uniform float TrackTime;
uniform float uVerh;
uniform sampler2D golTex;

in vec2 vTexCoord;

out vec4 Color;

float rand(vec2 co);
vec4 HSVAtoRGBA(vec4 HSVA);
vec4 blend(vec4 destination, vec4 source);
float mysinStep(float T1, float T2, float V1, float V2, float curT);

void main(void) {
	vec2 times = vec2(99971.364, 3843.386);//Aufteilung: Offset, Takt
	vec2 myTexCoord = vTexCoord.st*vec2(2.0,1.0)+vec2(-1.0,0.0);
	float vTrackTime = TrackTime;//+times.x;//nur f�r Tests ohne Scenen davor

	float sValue;
	if(vTrackTime < times.x+3*times.y)
		sValue = mysinStep(times.x, times.x+3*times.y, 0.8, 0.3, vTrackTime);
	else
		sValue = 0.3;

	float lilaV = smoothstep((vTrackTime-times.x)/(3.0*times.y),(vTrackTime-times.x)/(3.0*times.y)+0.3, distance(vec2(0.0, 0.8), myTexCoord*vec2(1.0, uVerh)))*(-1.0)+1.0;
	float vV = smoothstep((vTrackTime-times.x)/(3.25*times.y),(vTrackTime-times.x)/(3.25*times.y)+0.3, distance(vec2(0.0, 0.8), myTexCoord*vec2(1.0, uVerh)))*(-1.0)+1.0;
	float orangeV = smoothstep((vTrackTime-times.x)/(3.5*times.y),(vTrackTime-times.x)/(3.5*times.y)+0.3, distance(vec2(0.0, 0.75), myTexCoord*vec2(1.0, uVerh)))*(-1.0)+1.0;
	float blueV = smoothstep((vTrackTime-times.x)/(4.0*times.y),(vTrackTime-times.x)/(3.7*times.y)+0.3, distance(vec2(0.0, 0.7), myTexCoord*vec2(1.0, uVerh)))*(-1.0)+1.0;
	
	vec2 curSunPosition = vec2(0.0, (vTrackTime-times.x)/(4.0*times.y));
	float vValue =	smoothstep(0.8, 0.9, distance(curSunPosition, myTexCoord*vec2(1.0, uVerh))*(-1.0)+1.0);

	if(vTrackTime > times.x+7.0*times.y) {
		float newV = smoothstep(times.x+7.0*times.y,times.x+8.0*times.y, vTrackTime)*(-1.0)+1.0;
		lilaV *= newV;
		vV *= newV;
		orangeV *= newV;
		blueV *= newV;
		vValue *= newV;
		Color = vec4(0.0,0.0,0.0,1.0);
	}
	else
		Color = texture(golTex, vTexCoord);

	
	Color = blend(Color, HSVAtoRGBA(vec4(0.68, sValue, lilaV,lilaV*0.7)));
	Color = blend(Color, HSVAtoRGBA(vec4(0.68, sValue, vV,vV*0.7)));
	Color = blend(Color, HSVAtoRGBA(vec4(0.5/6.0, sValue, orangeV,orangeV*0.7)));
	Color = blend(Color, HSVAtoRGBA(vec4(0.5583, sValue, blueV,blueV*0.7)));
	Color = blend(Color,HSVAtoRGBA(vec4(0.12, sValue, vValue*1.25, vValue)));
}

float mysinStep(float T1, float T2, float V1, float V2, float curT) {
	float PI = 3.1415926535;
	float curSin = 0.5*sin(PI*(((curT-T1)/(T2-T1))-0.5))+0.5;
	float curV = curSin*(V2-V1)+V1;
	return curV;
}

vec4 blend(vec4 destination, vec4 source) {
	vec3 finalColor = vec3(1.0-source.a)*destination.rgb + source.rgb*vec3(source.a);
	return vec4(finalColor, 1.0);
}

float rand(vec2 co)
{
	return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

vec4 HSVAtoRGBA(vec4 HSVA)
{
	vec4 RGBAreturner;
	RGBAreturner.a = HSVA.w;
	HSVA.x *= 360.0;
	int hi = int((HSVA.x-mod(HSVA.x, 60.0))/60.0);
	float f = HSVA.x/60.0-hi;
	float p = HSVA.z*(1-HSVA.y);
	float q = HSVA.z*(1-HSVA.y*f);
	float t = HSVA.z*(1-HSVA.y*(1-f));
	
	switch(hi) {
	case 0:
	case 6:
		RGBAreturner.rgb = vec3(HSVA.z, t, p);
		break;
	case 1:
		RGBAreturner.rgb = vec3(q, HSVA.z, p);
		break;
	case 2:
		RGBAreturner.rgb = vec3(p, HSVA.z, t);
		break;
	case 3:
		RGBAreturner.rgb = vec3(p, q, HSVA.z);
		break;
	case 4:
		RGBAreturner.rgb = vec3(t, p, HSVA.z);
		break;
	case 5:
		RGBAreturner.rgb = vec3(HSVA.z, p, q);
		break;
	default:
		RGBAreturner.rgb = vec3(0.0);
		break;
	}
	return RGBAreturner;
}