#version 330

precision highp float;

in vec4 vVertex;
in vec2 vTexCoords;

out vec2 vTexCoord;

void main(void) {
	gl_Position = vVertex;
	gl_Position.z = 1.0;
	vTexCoord = vTexCoords;
}