#version 450 core

layout(location = 0) in vec4 v4Position;
layout(location = 1) in vec3 v3Normal;
layout(location = 2) in vec2 v2UVs;

smooth out vec2 v2vUVs;

void main(void) {
	v2vUVs = v2UVs;
	gl_Position = vec4(v4Position.xy, -1.0,1.0);
}