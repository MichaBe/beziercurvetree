#version 450 core

layout (location = 0) in vec4 position;
layout (location = 1) in vec3 controllpoints;
layout (location = 2) in float level;

layout (location = 0) uniform float fTime;

out VS_OUT {
	vec3 cp;
	float level;
} vs_out;

void main() {
	
	vs_out.cp = controllpoints;
	vs_out.level = level;
	gl_Position = position;
}