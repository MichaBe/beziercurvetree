#version 450 core

layout(lines) in;
layout(triangle_strip, max_vertices = 3) out;

layout (location = 3) uniform mat4 m4MVPR;
layout (location = 4) uniform mat3 m3RotOnly;
layout (location = 8) uniform float fSeed;

in TES_OUT {
	float level;
	vec3 v3Position;
	float fTessCoord;
} geom_in[];

out GEOM_OUT {
	float level;
	vec3 v3Position;
	float fPS;
	vec2 v2RandSeed;
	vec3 v3Normal;
} geom_out;

float rand(vec2 co)
{
	return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main(void) {
	vec2 seed1 = gl_in[0].gl_Position.xy*vec2(geom_in[0].level+fSeed);
	vec2 seed2 = gl_in[0].gl_Position.yz*vec2(gl_in[0].gl_Position.x, geom_in[0].level*fSeed);
	vec2 seed3 = vec2(fSeed,seed2.y);

	vec3 v3RAll = (vec3(rand(seed3),rand(seed1),rand(seed2))-vec3(0.5))*vec3(0.5);
	v3RAll *= vec3((gl_in[0].gl_Position.y+2.0)*0.5);

	vec4 P1 = vec4(gl_in[0].gl_Position.xyz+vec3(rand(seed1),rand(seed2),rand(seed3))*0.1, 1.0);

	seed1 = gl_in[1].gl_Position.xy*vec2(geom_in[1].level+fSeed);
	seed2 = gl_in[1].gl_Position.yz*vec2(gl_in[1].gl_Position.x, geom_in[1].level*fSeed);
	seed3 = vec2(fSeed,seed2.y);

	vec4 P2 = vec4(gl_in[1].gl_Position.xyz+vec3(rand(seed1),rand(seed2),rand(seed3))*0.1, 1.0);
	vec3 v3Zwischen = P1.xyz-P2.xyz;
	vec4 P3 = vec4(v3Zwischen*0.5+P1.xyz+vec3(v3Zwischen.y*-1.0, v3Zwischen.x, 0.0)*0.5+vec3(rand(seed2),rand(seed3),rand(seed1))*0.1,1.0);

	/*translate*/
	P1.xyz += v3RAll;
	P2.xyz += v3RAll;
	P3.xyz += v3RAll;
	
	vec3 v3MyNormal = m3RotOnly*cross(P1.xyz-P2.xyz, P1.xyz-P3.xyz);
	v3MyNormal = normalize(v3MyNormal*sign(-v3MyNormal.z));

	geom_out.v3Position = m3RotOnly*P1.xyz;
	gl_Position = m4MVPR*P1;
	geom_out.level = geom_in[0].level;
	geom_out.fPS = rand(vec2(seed1.x, fSeed))*5.0+1.0;
	geom_out.v2RandSeed = seed3;
	geom_out.v3Normal = v3MyNormal;
	EmitVertex();	

	geom_out.v3Position = m3RotOnly*P2.xyz;
	gl_Position = m4MVPR*P2;
	geom_out.level = geom_in[1].level;
	geom_out.fPS = rand(vec2(seed1.x, fSeed))*5.0+1.0;
	geom_out.v2RandSeed = seed3;
	geom_out.v3Normal = v3MyNormal;
	EmitVertex();

	geom_out.v3Position = m3RotOnly*P3.xyz;
	gl_Position = m4MVPR*P3;
	geom_out.level = geom_in[1].level;
	geom_out.fPS = rand(vec2(seed2.x, fSeed))*5.0+1.0;
	geom_out.v2RandSeed = seed3;
	geom_out.v3Normal = v3MyNormal;
	EmitVertex();
	
	EndPrimitive();
}