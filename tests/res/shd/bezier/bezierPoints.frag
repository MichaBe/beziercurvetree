#version 450 core

out vec4 color;

in GEOM_OUT {
	float level;
	vec3 v3Position;
	float fPS;
	vec2 v2RandSeed;
	vec3 v3Normal;
} fs_in;

float rand(vec2 co)
{
	return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main(void) {
	color = vec4(1.0);
	
	vec4 v4Hellgelb = vec4(0.961,0.961,0.1373,1.0);
	vec4 v4Hellrot = vec4(0.627,0.0275,0.004,1.0);

	v4Hellgelb.rgb *= vec3(0.8);
	v4Hellrot.rgb *= vec3(0.8);

	vec4 v4Dunkelblau = vec4(0.09,0.267,0.286, 1.0);

	vec4 v4DunkelGruen = vec4(
	0.027,
	0.063,
	0.051, 
	1.0);

	color = mix(
		mix(
			v4DunkelGruen,
			v4Dunkelblau,
			dot(fs_in.v3Position.xz,fs_in.v3Position.xz)*0.1
		),
		mix(
			v4Hellrot,
			v4Hellgelb,
			smoothstep(0.0,17.5,distance(fs_in.v3Position, vec3(0.0,0.35,0.0)))+rand(fs_in.v2RandSeed)*0.4-0.2
		),
		vec4(smoothstep(0.2,0.5,fs_in.v3Position.y))
		
	);
	color.a = smoothstep(-0.3,0.2,fs_in.v3Position.y);
	color.rgb -= vec3((smoothstep(-1.0,-8.0,fs_in.v3Position.z))*0.25);
	color.a -= (smoothstep(-5.0,0.0,fs_in.v3Position.z)*0.25);
	//color.rgb *= vec3(max(0.0,dot(fs_in.v3Normal, vec3(0.0,1.0,0.0))));
}