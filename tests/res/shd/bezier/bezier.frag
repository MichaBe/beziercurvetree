#version 450 core

out vec4 color;

in TES_OUT {
	float level;
	vec3 v3Position;
} fs_in;

void main(void) {
	color = vec4(196.0/255.0,
		1.0,
		251.0/255.0,
		1.0);
	color.rgb *= smoothstep(-0.2,0.4,fs_in.v3Position.y);
	color.a = smoothstep(1.0,-1.0, fs_in.v3Position.z);
	color.a *= 0.3;
}