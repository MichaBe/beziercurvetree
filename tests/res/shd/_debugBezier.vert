#version 450 core

layout (location = 0) in vec4 position;
layout (location = 1) in vec3 controllpoints;
layout (location = 2) in float level;

layout (location = 3) uniform int mode;
layout (location = 4) uniform mat4 m4MVPR;

layout (location = 5) uniform float fTime;
layout (location = 6) uniform vec3 v3Direction;


flat out vec4 pointColor;

mat3 rotationMatrix(vec3 axis, float angle);

void main(void) {
	if(mode == 0) {
		pointColor = vec4(1.0,0.0,0.0,1.0);
		

		gl_Position = m4MVPR*position;
	}
	else {
		pointColor = vec4(0.0,1.0,0.0,1.0);

		/*vec3 v3Temp1 = controllpoints-position.xyz;

		vec3 v3RotationsAchse = position.xyz-dot(v3Direction,position.xyz)*v3Direction;
	
		v3Temp1 = rotationMatrix(v3RotationsAchse, sin(fTime*0.001+level*4.0)+1.0)*v3Temp1;
		v3Temp1 += position.xyz;
		*/
		gl_Position = m4MVPR*position;/*vec4(v3Temp1,1.0);*/
	}
}

mat3 rotationMatrix(vec3 axis, float angle) {
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;
    
    return mat3(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c);
}