#version 450 core

flat in vec4 pointColor;

out vec4 color;

void main(void) {
	color = pointColor;
}