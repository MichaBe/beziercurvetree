#version 450 core

smooth in vec2 v2vUVs;

out vec4 v4Color;

void main(void) {
	vec2 v2ToTest = vec2(v2vUVs.x, (v2vUVs.y-0.65)*0.2+0.65);
	float d1 = distance(v2ToTest,vec2(0.35,0.65));
	float d2 = distance(v2ToTest,vec2(0.5, 0.65));
	float d3 = distance(v2ToTest,vec2(0.85,0.65));
	float d4 = distance(v2ToTest,vec2(0.7,0.65));

	float fForSmooth = min(min(min(d1,d2),d3),d4)*4.0;

	vec4 v4Hell = vec4(196.0/255.0,
		1.0,
		251.0/255.0,
		1.0);
	vec4 v4Dunkel = vec4(77.0/255.0,
		128.0/255.0,
		145.0/255.0,
		1.0);
	vec4 v4Boden = vec4(14.0/255.0,
		16.0/255.0,
		22.0/255.0,
		1.0);
	vec2 v2ToTest2 = vec2((v2vUVs.x-0.5)*0.4+0.5, (v2vUVs.y-0.5)*1.5+0.5);
	v4Color = mix(v4Hell,v4Dunkel,vec4(smoothstep(0.0,0.4,fForSmooth)));
	v4Color = mix(v4Boden,v4Color,vec4(smoothstep(0.4,0.8,distance(vec2(1.0,0.5),v2ToTest2))*-1.0+1.0));
}