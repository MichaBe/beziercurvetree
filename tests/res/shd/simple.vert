#version 450 core

layout (location = 0) in vec3 position;
layout (location = 1) in float fIndex;

layout (location = 1) uniform mat4 m4MVPR;

smooth out float fIndexO;

void main(void) {
	fIndexO = fIndex;
	gl_Position = m4MVPR*vec4(position, 1.0);
}