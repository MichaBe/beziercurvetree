#include "stdafx.h"
#include "Knoten.h"


CKnoten::CKnoten(int iVorgaengerID) {
	m_iNachfolgeranzahl = 0;
	m_iVorgaenger = iVorgaengerID;
	m_iOwnID = 0;
}

CKnoten::~CKnoten() {

}

int CKnoten::GetNachfolger() {
	return m_iNachfolgeranzahl;
}

int CKnoten::CalcNachfolger() {
	int iReturner = 0;
	if (m_pNachfolger.size() == 0) {
		m_iNachfolgeranzahl = 0;
		iReturner = 1;
	}
	else {
		for (int i = 0; i < m_pNachfolger.size(); ++i)
			iReturner += m_pNachfolger[i]->CalcNachfolger();
		m_iNachfolgeranzahl = iReturner;
	}
	return iReturner;
}

void CKnoten::AddNachfolger(CKnoten* pNachfolger) {
	m_pNachfolger.push_back(pNachfolger);
}