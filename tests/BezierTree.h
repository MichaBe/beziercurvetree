#pragma once

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <mbc_demo.h>
#include <iostream>
#include <vector>
#include "Knoten.h"

#include <fstream>
using namespace std;
using namespace glm;

extern mbc_demo* myDemo;

namespace BezierTree {
	void initObject();
	void initShader();

	struct objectData {
		GLuint vao;
		GLuint vbo;
		GLuint vCount;
		GLuint vao2;
		GLuint vCount2;
	} BT;
	bool bDrawPoints;
	vector<vec4> vertices;
	vector<vec3> controllpoint;
	vector<float> levels;
	struct oneLine {
		vec3 first;
		vec3 second;
	};
	vector<oneLine> lines;
	vector<float> countXes;
	vector<CKnoten*> meineKnoten;
	map<int, vector<int>> endenToConnect;

	struct shaderData {
		GLuint shaders[4];
		GLuint programm;
	} SHD, SHDPoints;

	mat4 rot;
	float TLO1;
	int iCurSeed;
	void init(double dt) {
		srand(1000);
		bDrawPoints = false;
		initObject();
		initShader();
		rot = glm::rotate(mat4(1.0), 150.0f, glm::vec3(0.0, 1.0, 0.0));
		TLO1 = 17.0f;
	}
	bool bDebugDraw = false;
	bool bOffscreen = false;
	float fSeed1;

	mat4 m4MVPR;
	mat3 m3RotOnly;
	
	void pre(double dt) {
		iCurSeed = 131003005;
		fSeed1 = 0.796875;
		
		myDemo->contextManager->connectKey(GLFW_KEY_2, &bDebugDraw);
		myDemo->contextManager->connectKey(GLFW_KEY_Q, [] {fSeed1 = fmod(timeGetTime() / 1000.0f, 1.0f); cout << fSeed1 << endl; });
		myDemo->contextManager->connectKey(GLFW_KEY_W, [] {TLO1 = std::min(TLO1 + 1.0f, 17.0f); });
		myDemo->contextManager->connectKey(GLFW_KEY_S, [] {TLO1 = std::max(1.0f, TLO1 - 1.0f); });
		myDemo->contextManager->connectKey(GLFW_KEY_R, &bOffscreen);

		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_PROGRAM_POINT_SIZE);
		glLineWidth(1.0f);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glPatchParameteri(GL_PATCH_VERTICES, 2);
		glClearColor(0.7, 0.2, 0.2, 1.0);
		
		myDemo->cameraManager->calcFrame(5.0f);
		m4MVPR = myDemo->cameraManager->vpMatrix*glm::scale(mat4(1.0f), vec3(1.5f))*glm::translate(mat4(1.0f), vec3(-3.0, -0.1f, 0.0))*rot;
		m3RotOnly = mat3(glm::transpose(glm::inverse(m4MVPR)));

	}
	void calc(double dt) { }

	void screenshot() {
		int row_size = ((1920 * 3 + 3)&~3);
		int data_size = row_size * 1080;
		unsigned char* data = new unsigned char[data_size];
#pragma pack (push, 1)
		struct {
			unsigned char id;
			unsigned char cmap;
			unsigned char imagetype;
			short start;
			short mapsize;
			unsigned char cmapbpp;
			short xorg;
			short yorg;
			short width;
			short height;
			unsigned char bpp;
			unsigned char descriptor;
		} tga_header;
#pragma pack (pop)

		glReadPixels(0, 0, 1920, 1080, GL_BGR, GL_UNSIGNED_BYTE, data);
		memset(&tga_header, 0, sizeof(tga_header));
		tga_header.imagetype = 2;
		tga_header.width = 1920;
		tga_header.height = 1080;
		tga_header.bpp = 24;

		ofstream myFile("screenshot.tga", std::ios::binary);
		myFile.write((char*)&tga_header, sizeof(tga_header));
		myFile.write((char*)data, data_size);
		myFile.close();
	}

	void rend(double dt) {
		if (bOffscreen) {
			screenshot();
			bOffscreen = false;
		}
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Draw background
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		myDemo->shaderManager->useProgram("background");
		myDemo->objectManager->drawObject("_normPlane");

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBlendEquation(GL_FUNC_ADD);

		//draw treeangles
		glEnable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		glBindVertexArray(BT.vao);
		glUseProgram(SHDPoints.programm);
		glUniform1f(2, TLO1);
		glUniformMatrix4fv(3, 1, false, glm::value_ptr(m4MVPR));
		glUniformMatrix3fv(4, 1, false, glm::value_ptr(m3RotOnly));
		glUniform1f(8, fSeed1);
		glDrawArrays(GL_PATCHES, 0, BT.vCount);
		

		glDisable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);

		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		//Draw tree
		glBindVertexArray(BT.vao);
		glUseProgram(SHD.programm);
		glUniform1f(0, dt);
		glUniform1f(2, TLO1);
		glUniformMatrix4fv(3, 1, false, glm::value_ptr(m4MVPR));
		glUniformMatrix3fv(4, 1, false, glm::value_ptr(m3RotOnly));
		glDrawArrays(GL_PATCHES, 0, BT.vCount);
		
		glDisable(GL_BLEND);

		//Begin Debugdraw
		if (bDebugDraw) {
			myDemo->shaderManager->useProgram("_debugBezier");
			glUniform1i(3, 0);

			glUniform1f(5, dt);
			glUniformMatrix4fv(4, 1, false, glm::value_ptr(m4MVPR));
			glDrawArrays(GL_PATCHES, 0, BT.vCount);
			myDemo->shaderManager->useProgram("_debugBezier");
			glUniform1i(3, 1);

			glUniform1f(5, dt);
			glUniformMatrix4fv(4, 1, false, glm::value_ptr(m4MVPR));
			glDrawArrays(GL_PATCHES, 0, BT.vCount);
			//End Debugdraw

			myDemo->shaderManager->useProgram("simple");
			glBindVertexArray(BT.vao2);
			glUniformMatrix4fv(1, 1, false, glm::value_ptr(m4MVPR));
			glDrawArrays(GL_LINES, 0, BT.vCount2);
		}
		glBindVertexArray(0);
	}
	void clean(double dt){
		glDeleteBuffers(1, &BT.vao);
		glDeleteBuffers(1, &BT.vao2);
		glDeleteProgram(SHD.programm);
		for (int i = 0; i < meineKnoten.size(); ++i)
			delete meineKnoten[i];
	}

	float changer(float fRadius1, float fRadius2, float fCurPoint) {
		return fRadius1 + (fRadius2 - fRadius1)*fCurPoint/**fCurPoint*/+std::sinf(fCurPoint*mbc::PI*9.0f)*0.1f;
		
	}
	void GenSpiral(int iOwnID, int iVorgaengerID, vec3 v3V1, vec3 v3V2, int iCircleCount, float fStartRotation, float fRadius1, float fRadius2) {
		
		float fLength = sqrtf((v3V2.x - v3V1.x)*(v3V2.x - v3V1.x) +
			(v3V2.y - v3V1.y)*(v3V2.y - v3V1.y) + (v3V2.z - v3V1.z)*(v3V2.z - v3V1.z));
		
		float fHeightPerBezier = fLength / (iCircleCount*4.0f);
		vec3 v3Direction = normalize(v3V2 - v3V1);

		vec3 av3Predecessor[4];
		if (v3Direction.z != 0.0f){
			av3Predecessor[0] = normalize(vec3(0.0f, 1.0f, -v3Direction.y / v3Direction.z));
		}
		else if (v3Direction.y != 0.0f) {
			av3Predecessor[0] = normalize(vec3(0.0f, -v3Direction.z / v3Direction.y, 1.0f));
		}
		else {
			av3Predecessor[0] = vec3(0.0f, 1.0f, 0.0f);
		}
		av3Predecessor[0] = vec3(glm::rotate(mat4(1.0),fStartRotation, v3Direction)*vec4(av3Predecessor[0], 1.0f));
		

		av3Predecessor[1] = normalize(cross(v3Direction, av3Predecessor[0]));
		av3Predecessor[2] = normalize(cross(v3Direction, av3Predecessor[1]));
		av3Predecessor[3] = normalize(cross(v3Direction, av3Predecessor[2]));

		float fVaryingRadius = fRadius1;
		float fOldR = fVaryingRadius;
		const float fMK = 0.551915024494f;
		int i = 0;
		endenToConnect[iVorgaengerID + 100].push_back(vertices.size());
		for (; i < iCircleCount * 4; ++i) {
			fOldR = fVaryingRadius;
			fVaryingRadius = changer(fRadius1, fRadius2, i / (iCircleCount * 4.0f));

			vec3 v3UntererPunkt = av3Predecessor[i % 4] * fOldR + v3V1 + v3Direction*fHeightPerBezier*float(i);
			vertices.push_back(vec4(v3UntererPunkt,1.0f));

			vec3 v3ObererPunkt = av3Predecessor[(i+1) % 4] * fVaryingRadius + v3V1 + v3Direction*fHeightPerBezier*float(i+1);
			vertices.push_back(vec4(v3ObererPunkt, 1.0f));


			controllpoint.push_back(v3UntererPunkt + av3Predecessor[(i + 1) % 4] * fMK*fOldR+v3Direction*((2.0f*fMK*fHeightPerBezier)/mbc::PI));
			controllpoint.push_back(v3ObererPunkt + av3Predecessor[i % 4] * fMK*fVaryingRadius - v3Direction*((2.0f*fMK*fHeightPerBezier) / mbc::PI));

			levels.push_back(i / float(iCircleCount * 4));
			levels.push_back(i / float(iCircleCount * 4));
		}
		endenToConnect[iOwnID].push_back(vertices.size() - 1);
	}

	//Generates the Tree
	vec3 GenLine(vec3 v3Point, vec3 v3Dir, float fCurY) {
		
		vec3 v3NewDir = v3Dir;
		
		v3NewDir.x = v3NewDir.x + (((rand() % 1000) - 500) / 500.0f)*(1.0f - fCurY/3.0f)*2.0f;
		v3NewDir.z = v3NewDir.z + (((rand() % 1000) - 500) / 500.0f)*(1.0f - fCurY/3.0f)*2.0f;
		
		v3NewDir.y *= 1.3f;

		v3NewDir = normalize(v3NewDir)*(1.0f-fCurY+0.3f);

		vec3 v3NewPoint = v3Point + v3NewDir;

		return v3NewPoint;
	}
	void GenTree(int seed, int iCountX) {
		srand(seed);

		int iFibo1 = 1, iFibo2 = 1;
		oneLine tempLine;
		tempLine.first = vec3(0.0f, -1.0f, 0.0f);
		tempLine.second = vec3(0.0f, 1.0f, 0.0f);
		lines.push_back(tempLine);
		countXes.push_back(2.0f);
		meineKnoten.push_back(new CKnoten(-1));
		meineKnoten[meineKnoten.size() - 1]->SetOwnID(meineKnoten.size() - 1);
		tempLine.first = vec3(0.0f, 1.0f, 0.0f);
		tempLine.second = vec3(0.0f, 1.7f, 0.0f);
		lines.push_back(tempLine);
		countXes.push_back(3.0f);
		meineKnoten.push_back(new CKnoten(0));
		meineKnoten[meineKnoten.size() - 1]->SetOwnID(meineKnoten.size() - 1);
		meineKnoten[0]->AddNachfolger(meineKnoten[meineKnoten.size() - 1]);
		for (int i = 0; i < iCountX; ++i) {
			int iCurFibo = iFibo1 + iFibo2;

			int iCurSize = lines.size();
			for (int j = 0; j < iFibo2; ++j) {
				int iIndexPrev = iCurSize - j - 1;
				oneLine pCurLine = lines[iIndexPrev];
				vec3 v3CurDir = normalize(pCurLine.second - pCurLine.first);
				tempLine.first = pCurLine.second;
				tempLine.second = GenLine(pCurLine.second,v3CurDir, i/float(iCountX));
				lines.push_back(tempLine);
				countXes.push_back(countXes[iIndexPrev] + 1);
				meineKnoten.push_back(new CKnoten(iIndexPrev));
				meineKnoten[meineKnoten.size() - 1]->SetOwnID(meineKnoten.size() - 1);
				meineKnoten[iIndexPrev]->AddNachfolger(meineKnoten[meineKnoten.size() - 1]);
			}
			for (int j = 0; j < iCurFibo - iFibo2; ++j) {
				int iIndexPrev = iCurSize - iFibo2 - (float(rand()%1000) / 1000.0f)*(iCurFibo - iFibo2);
				oneLine pCurLine = lines[iIndexPrev];
				vec3 v3CurDir = normalize(pCurLine.second - pCurLine.first);
				tempLine.first = pCurLine.second;
				tempLine.second = GenLine(pCurLine.second, v3CurDir, i/float(iCountX));
				lines.push_back(tempLine);
				countXes.push_back(countXes[iIndexPrev] + 1.0f);
				meineKnoten.push_back(new CKnoten(iIndexPrev));
				meineKnoten[meineKnoten.size() - 1]->SetOwnID(meineKnoten.size() - 1);
				meineKnoten[iIndexPrev]->AddNachfolger(meineKnoten[meineKnoten.size() - 1]);
			}
			iFibo1 = iFibo2;
			iFibo2 = iCurFibo;
		}
		meineKnoten[0]->CalcNachfolger();
		cout << meineKnoten[0]->GetNachfolger();
	}

	
	void initObject() {
		int iLevelsToCreate = 7;
		GenTree(iCurSeed, iLevelsToCreate);
		
		for (int i = 0; i < lines.size(); ++i) {
			float fLength = dot(lines[i].first-lines[i].second,lines[i].first-lines[i].second);
			
			float fLev1 = 2.0f/std::max(1.0f, (countXes[i] * countXes[i]));
			float fLev2 = 2.0f/std::max(1.0f,((countXes[i] + 1)*(countXes[i] + 1)));
			
			float fDegreeRotation = 360.0f / std::max(1,meineKnoten[i]->GetNachfolger());
			for (int j = 0; j < std::max(1,meineKnoten[i]->GetNachfolger()); ++j) {
				float fRandPart = ((rand() % 1000) / 1000.0f) * 0.3f-0.15f;
				int iVorgaengerID = meineKnoten[i]->GetVorgaengerID();

				GenSpiral(meineKnoten[i]->GetOwnID(),iVorgaengerID, lines[i].first, lines[i].second, std::max(1.0f,(fLength*5.0f) / ((meineKnoten[i]->GetNachfolger() + 1))), fDegreeRotation*j, fLev1+fRandPart, fLev2+fRandPart);
			}
		}
		//end- und anfangspunkte m�ssen gespeichert sein, sodass sie verbunden werden k�nnen.
		auto myIt = endenToConnect.begin();
		for (; myIt != endenToConnect.end(); ++myIt) {
			if ((*myIt).first > 99) {
				int iIndexUntererPunkt = (*myIt).first - 100;
				for (int i = 0; i < (*myIt).second.size(); ++i) {
					
					vertices.push_back(vertices[endenToConnect[iIndexUntererPunkt][i]]);
					vec3 newP = vec3(vertices[endenToConnect[iIndexUntererPunkt][i]]) + (vec3(vertices[endenToConnect[iIndexUntererPunkt][i]]) - controllpoint[endenToConnect[iIndexUntererPunkt][i]]);
					controllpoint.push_back(newP);
					
					vertices.push_back(vertices[(*myIt).second[i]]);
					vec3 newP2 = vec3(vertices[(*myIt).second[i]]) + (vec3(vertices[(*myIt).second[i]]) - controllpoint[(*myIt).second[i]]);
					controllpoint.push_back(newP2);
				}
			}
			else if ((*myIt).first == 99) {
				for (int i = 0; i < (*myIt).second.size(); ++i) {
					vertices[(*myIt).second[i]] *= vec4(3.0f, 1.0f, 3.0f, 1.0f);
					controllpoint[(*myIt).second[i]].x = controllpoint[(*myIt).second[i]].z = 0.0f;
				}
			}
		}


		///////////////////////////////////////////////////
		GLuint uiTempvbo;
		glGenVertexArrays(1, &BT.vao2);
		glBindVertexArray(BT.vao2);

		glGenBuffers(1, &uiTempvbo);
		glBindBuffer(GL_ARRAY_BUFFER, uiTempvbo);
		glBufferData(GL_ARRAY_BUFFER, lines.size()*sizeof(glm::vec3) * 2 + countXes.size()*sizeof(float), NULL, GL_STATIC_DRAW);
		glBufferSubData(GL_ARRAY_BUFFER, 0, lines.size()*sizeof(glm::vec3) * 2, lines.data());
		glBufferSubData(GL_ARRAY_BUFFER, lines.size()*sizeof(glm::vec3) * 2, countXes.size()*sizeof(float), countXes.data());
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, 0, (GLvoid*)(lines.size()*sizeof(glm::vec3) * 2));
		
		glBindVertexArray(0);
		glDeleteBuffers(1, &uiTempvbo);
		///////////////////////////////////////////////////
		glGenVertexArrays(1, &BT.vao);
		glBindVertexArray(BT.vao);

		glGenBuffers(1, &BT.vbo);
		glBindBuffer(GL_ARRAY_BUFFER, BT.vbo);
		glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(glm::vec4) + controllpoint.size()*sizeof(glm::vec3)+levels.size()*sizeof(float), NULL, GL_STATIC_DRAW);
		glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.size()*sizeof(glm::vec4), vertices.data());
		glBufferSubData(GL_ARRAY_BUFFER, vertices.size()*sizeof(glm::vec4), controllpoint.size()*sizeof(glm::vec3), controllpoint.data());
		glBufferSubData(GL_ARRAY_BUFFER, vertices.size()*sizeof(glm::vec4) + controllpoint.size()*sizeof(glm::vec3), levels.size()*sizeof(float), levels.data());

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)(vertices.size()*sizeof(glm::vec4)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, 0, (GLvoid*)(vertices.size()*sizeof(glm::vec4) + controllpoint.size()*sizeof(glm::vec3)));
		
		glBindVertexArray(0);
		
		glDeleteBuffers(1, &BT.vbo);

		BT.vCount = vertices.size();
		BT.vCount2 = lines.size() * 2;
		cout <<endl<< BT.vCount<<endl;
	}
	
	void initShader() {
		SHD.shaders[0] = glCreateShader(GL_VERTEX_SHADER);
		SHD.shaders[1] = glCreateShader(GL_TESS_CONTROL_SHADER);
		SHD.shaders[2] = glCreateShader(GL_TESS_EVALUATION_SHADER);
		SHD.shaders[3] = glCreateShader(GL_FRAGMENT_SHADER);
		SHD.programm = glCreateProgram();

		string path[4] = { "res/shd/bezier/bezier.vert", "res/shd/bezier/bezier.tcs", "res/shd/bezier/bezier.tes", "res/shd/bezier/bezier.frag" };
		char tempInfolog[1024];
		for (int i = 0; i < 4; i++){
			std::fstream myStream;
			myStream.open(path[i], std::ios::in | std::ios::binary);
			std::ios::pos_type start = myStream.tellg();
			myStream.seekg(0, std::ios::end);
			std::ios::pos_type end = myStream.tellg();
			myStream.seekg(0, std::ios::beg);
			const int size = end - start;
			char* shaderContent = new char[size + 1];
			myStream.read(shaderContent, size);
			shaderContent[size] = '\0';
			myStream.close();

			char* fsStringPtr[1];
			fsStringPtr[0] = (char *)shaderContent;
			glShaderSource(SHD.shaders[i], 1, (const GLchar **)fsStringPtr, NULL);
			delete(shaderContent);
			glCompileShader(SHD.shaders[i]);
			
			glGetShaderInfoLog(SHD.shaders[i], 1024, NULL, tempInfolog);
			cout << endl << path[i] << endl;
			for (int j = 0; tempInfolog[j] != '\0'; ++j)
				cout << tempInfolog[j];
			cout << endl;
			glAttachShader(SHD.programm, SHD.shaders[i]);
		}
		
		glLinkProgram(SHD.programm);
		glGetProgramInfoLog(SHD.programm, 1024, NULL, tempInfolog);
		cout << endl << "Programm:" << endl;
		for (int i = 0; tempInfolog[i] != '\0'; ++i)
			cout << tempInfolog[i];
		cout << endl;

		for (int i = 0; i < 4; ++i)
			glDeleteShader(SHD.shaders[i]);

		//////////////////////////////////////////////////////////

		SHDPoints.shaders[0] = glCreateShader(GL_VERTEX_SHADER);
		SHDPoints.shaders[1] = glCreateShader(GL_TESS_CONTROL_SHADER);
		SHDPoints.shaders[2] = glCreateShader(GL_TESS_EVALUATION_SHADER);
		GLuint shdGeom = glCreateShader(GL_GEOMETRY_SHADER);
		SHDPoints.shaders[3] = glCreateShader(GL_FRAGMENT_SHADER);
		SHDPoints.programm = glCreateProgram();

		string path2[5] = { "res/shd/bezier/bezierPoints.vert", "res/shd/bezier/bezierPoints.tcs", "res/shd/bezier/bezierPoints.tes", "res/shd/bezier/bezierPoints.frag", "res/shd/bezier/bezierPoints.geom" };
		
		for (int i = 0; i < 4; i++){
			std::fstream myStream;
			myStream.open(path2[i], std::ios::in | std::ios::binary);
			std::ios::pos_type start = myStream.tellg();
			myStream.seekg(0, std::ios::end);
			std::ios::pos_type end = myStream.tellg();
			myStream.seekg(0, std::ios::beg);
			const int size = end - start;
			char* shaderContent = new char[size + 1];
			myStream.read(shaderContent, size);
			shaderContent[size] = '\0';
			myStream.close();

			char* fsStringPtr[1];
			fsStringPtr[0] = (char *)shaderContent;
			glShaderSource(SHDPoints.shaders[i], 1, (const GLchar **)fsStringPtr, NULL);
			delete(shaderContent);
			glCompileShader(SHDPoints.shaders[i]);

			glGetShaderInfoLog(SHDPoints.shaders[i], 1024, NULL, tempInfolog);
			cout << endl << path2[i] << endl;
			for (int j = 0; tempInfolog[j] != '\0'; ++j)
				cout << tempInfolog[j];
			cout << endl;
			glAttachShader(SHDPoints.programm, SHDPoints.shaders[i]);
		}

		std::fstream myStream;
		myStream.open(path2[4], std::ios::in | std::ios::binary);
		std::ios::pos_type start = myStream.tellg();
		myStream.seekg(0, std::ios::end);
		std::ios::pos_type end = myStream.tellg();
		myStream.seekg(0, std::ios::beg);
		const int size = end - start;
		char* shaderContent = new char[size + 1];
		myStream.read(shaderContent, size);
		shaderContent[size] = '\0';
		myStream.close();

		char* fsStringPtr[1];
		fsStringPtr[0] = (char *)shaderContent;
		glShaderSource(shdGeom, 1, (const GLchar **)fsStringPtr, NULL);
		delete(shaderContent);
		glCompileShader(shdGeom);

		glGetShaderInfoLog(shdGeom, 1024, NULL, tempInfolog);
		cout << endl << path2[4] << endl;
		for (int j = 0; tempInfolog[j] != '\0'; ++j)
			cout << tempInfolog[j];
		cout << endl;
		glAttachShader(SHDPoints.programm, shdGeom);

		glLinkProgram(SHDPoints.programm);
		glGetProgramInfoLog(SHDPoints.programm, 1024, NULL, tempInfolog);
		cout << endl << "Programm:" << endl;
		for (int i = 0; tempInfolog[i] != '\0'; ++i)
			cout << tempInfolog[i];
		cout << endl;

		for (int i = 0; i < 4; ++i)
			glDeleteShader(SHDPoints.shaders[i]);
		glDeleteShader(shdGeom);
	}
}