#include "stdafx.h"
#include <Windows.h>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <mbc_demo.h>
#include <mbc.h>
#include "BezierTree.h"
#include "OpenGL.h"

mbc_demo* myDemo;

int main() {
	myDemo = new mbc_demo("res", mbc::DISABLE_SOUNDTRACK);

	myDemo->registerFunction("bezierTree", BezierTree::init, mbc::INITFUNCTION_TYPE);
	myDemo->registerFunction("bezierTree", BezierTree::pre, mbc::PREFUNCTION_TYPE);
	myDemo->registerFunction("bezierTree", BezierTree::calc, mbc::CALCFUNCTION_TYPE);
	myDemo->registerFunction("bezierTree", BezierTree::rend, mbc::RENDERFUNCTION_TYPE);
	myDemo->registerFunction("bezierTree", BezierTree::clean, mbc::CLEANFUNCTION_TYPE);
	
	myDemo->initScenes();
	myDemo->playDemo();

	myDemo->~mbc_demo();

	std::cout <<std::endl;
	//system("PAUSE");
	return 0;
}