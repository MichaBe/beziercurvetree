#pragma once

#include <vector>
#include <algorithm>

class CKnoten
{
public:
	CKnoten(int iVorgaengerID);
	~CKnoten();

	int GetVorgaengerID() { return m_iVorgaenger; };
	void SetOwnID(int iOwnID) { m_iOwnID = iOwnID; };
	int GetOwnID(){ return m_iOwnID; };
	int GetNachfolger();
	int CalcNachfolger();
	void AddNachfolger(CKnoten* pNachfolger);

private:
	std::vector<CKnoten*> m_pNachfolger;
	int m_iNachfolgeranzahl;
	int m_iVorgaenger;
	int m_iOwnID;
};

