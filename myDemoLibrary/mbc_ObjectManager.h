#pragma once

#include <rapidxml.hpp>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <array>
#include <functional>
#include <map>
#include <GL\glew.h>
#include <GL\wglew.h>
#include <glm\glm.hpp>
#include <iostream>
#include "mbc.h"

class mbc_ObjectManager
{
public:
	mbc_ObjectManager(rapidxml::xml_node<>* objectManager, std::string projectPath, std::fstream* log, bool logQ);
	~mbc_ObjectManager(void);

	void drawObject(std::string object, GLenum mode = GL_TRIANGLES);
	void drawObjectInstanced(std::string object, int count, GLenum mode = GL_TRIANGLES);
	void drawObject(mbc::returner returner, GLenum mode = GL_TRIANGLES);
	void drawObjectInstanced(mbc::returner returner, int count, GLenum mode = GL_TRIANGLES);
	
	int registerProcedure(std::string name, std::function<mbc::returner()> procedure);
	
	mbc::keepDesc getKeepData(std::string name);

	int getVertexCount(std::string object);

private:
	bool mbc_LoadObjFile(std::string file, std::string name, bool keep);
	static mbc::returner mbc_LoadNormPlane();

	std::map<std::string, GLuint> Objects;
	std::map<GLuint, int> VertexCount;
	std::map<GLuint, bool> isIndexed;

	std::map<std::string, std::vector<glm::vec3>> VerticesKeep;
	std::map<std::string, std::vector<int>> IndicesKeep;
	std::map<std::string, mbc::keepDesc> DescriptionKeep;
	std::string path;
};