#pragma once

#include <GL\glew.h>
#include <GL\wglew.h>
#include <vector>
#include <map>
#include <string>
#include "mbc.h"

class mbc_imageDescription {
public:
	GLenum type, wrap, filter, attachment;
	int channels;
	mbc_imageDescription(GLenum ptype, GLenum pwrap, GLenum pfilter, GLenum pattachment, int pchannels) {
		type = ptype;
		wrap = pwrap;
		filter = pfilter;
		attachment = pattachment;
		channels = pchannels;
	};
};

class mbc_RendertargetManager
{
public:
	mbc_RendertargetManager(std::pair<int, int> windowS);
	~mbc_RendertargetManager();
	int createRenderTarget(std::pair<int,int> RTD, std::vector<mbc_imageDescription> ID);
	void bindRenderTarget(int ID);
	int bindToSpace(GLuint FBO, GLenum attachment);

private:
	int firstTEXnum;
	int curTexCount;
	int maxTexCount;
	int curRenderTarget;
	std::pair<int, int> windowsize;
	std::vector<GLuint> fbos;
	std::vector<GLuint> texSpaces;	//nur f�rs l�schen wichtig
	std::map<std::pair<GLuint, GLenum>, int> texBindings; //<FBO, COLOR_ATTACHMENT>, 20-32
	std::map<GLuint, std::vector<GLenum>> attachments; //Liste der Attachments f�r einen FBO
	std::vector<GLuint> rbos;	//nur f�rs l�schen wichtig
	std::map<GLuint, std::pair<int,int>> sizes;
};