#pragma once

//--------------------------------------------------------------------------------------------
//  Headers
//--------------------------------------------------------------------------------------------

#include <windows.h>
#include <string>
#include <gl\glew.h>
#include <gl\wglew.h>

class GLWindow
{	
	friend class GLSystem;

public:
	GLWindow();
	~GLWindow();

	bool init();		
	bool kill();
			
	bool createWindow(int w, int h, int b, bool screen, bool onTop, int fsaa, int frequency);

	bool getFullscreen();
	bool getActive();
	unsigned int getWidth();
	unsigned int getHeight();
	HWND getHandle();
	HINSTANCE inst();
	HDC getHDC();

	
	
	void setWindowTitle(const std::string& title);

	friend LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	bool getKeyDown(int i);
	bool getKeyPressed(int i);

	bool	m_leftMouse;
	bool	m_rightMouse;
	float	m_mouseX, m_mouseY;
	float	m_mouseWheel;
			

private:

	unsigned int m_bpp, m_zbpp, m_sbpp;		// Bit Depths for buffers
	unsigned int m_width, m_height;			// Window width and height
	bool m_fullscreen;					// Fullscreen flag
	char *m_className;					// Class name for registeration

	int m_fsaa;							// Fullscreenalias multisamples
	bool m_onTop;							// Always-on-top 
	bool m_active;

	bool m_keysPressed[256];
	bool m_keysDown[256];

	HWND m_hwnd;
	HINSTANCE m_hinstance;
	HDC m_hdc;
	HGLRC m_hrc;

	int fetchFSAAMode(HDC hdc,int suggestedFormat, PIXELFORMATDESCRIPTOR p, int requestedmultisamples);
};

	//--------------------------------------------------------------------------------------------
	//  OpenGL System class
	//--------------------------------------------------------------------------------------------

	class GLSystem
	{
		friend class GLWindow;

		public:

			GLSystem();
			~GLSystem();

		private:		
			float m_zNear, m_zFar, m_fov;
			int m_width, m_height;			
	};
