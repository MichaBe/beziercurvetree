#pragma once

#include <rapidxml.hpp>
#include <fstream>
#include <functional>
#include <string>
#include <map>
#include <vector>
#include "mbc.h"

class mbc_SyncManager
{
public:
	mbc_SyncManager(rapidxml::xml_node<>* syncManager, std::fstream* log, bool logQ);
	~mbc_SyncManager(void);

	float getEvent(std::string name, float curTime);		//returns time to the event (as - value) or time since the event (as + value)
	bool getOnetimeEvent(std::string name, float curTime);
	//float getTrigger(std::string name, float curTime, float V1, float V2, int type = mbc::LIN); DEPRECATED
	float getTrigger(std::string name, float curTime, std::function<float(float, float, float)> stepFunction, bool alternating = false);

private:

	std::map<std::string,std::vector<float>> triggers;	//name -> start(0) | duration(1) | repeat(2)
	std::map<std::string,float> events;
	std::map<std::string,bool> requestedOneTimeEvent;
};

