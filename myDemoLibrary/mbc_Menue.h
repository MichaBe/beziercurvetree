#pragma once

#include <Windows.h>
#include <iostream>
#include <map>
#include <string>
#include <math.h>
#include <conio.h>

class mbc_Menue
{
public:
	mbc_Menue(std::string title);
	~mbc_Menue(void);

	std::pair<int,bool> showMenue();
	std::pair<int,int> getSize(int handle);

	void setCustom(bool custom);
	void addMenueItem(std::pair<int,int> Item, int Handle = 0);
private:
	std::string Title;
	std::map<int,std::pair<int,int>> menueItem;
	bool fullCustomizedMenue;
	int curMenueItemCount;
	void setDefaultParams();

	std::string centerize(std::string inputstring, int finalLength);
};

