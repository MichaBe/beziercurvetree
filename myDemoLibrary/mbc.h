#pragma once

#include <GL\glew.h>
#include <GL\wglew.h>
#include <glm\glm.hpp>

namespace mbc {
	//CameraManager u. SyncManager
	const static int LIN = 51;
	const static int SIN = 52;
	const static int STEP = 53;
	const static int ALT_LIN = 54;
	const static int ALT_SIN = 55;

	//Color
	const static int RGB1 = 1;
	const static int HSV1 = 2;

	//demo
	const static int RENDERFUNCTION_TYPE = 1;
	const static int CALCFUNCTION_TYPE = 2;
	const static int INITFUNCTION_TYPE = 3;
	const static int PREFUNCTION_TYPE = 4;
	const static int CLEANFUNCTION_TYPE = 5;
	const static unsigned int DISABLE_OBJECTS = 1;//!!
	const static unsigned int DISABLE_TEXTURES = 2;//!!
	const static unsigned int DISABLE_SHADERS = 4;//!!
	const static unsigned int DISABLE_SOUNDTRACK = 8;//!!
	const static unsigned int DISABLE_SYNC = 16;//!!
	const static unsigned int DISABLE_CAMERA = 32;//!!
	const static unsigned int MANIFEST_FROM_VAR = 64;//!!

	//MathManager
	const static float PI = 3.1415926535897932384626433832795f;

	//ObjectManager
	const static int IS_OK = 11;
	const static int WRONG_NAME = 12;
	struct returner {
		GLuint VAO;				//ID des ARRAY_BUFFERs
		int v_draw_Count;		//Anzahl der zu zeichnenden Punkte
		bool indexed;			//True: VAO hat einen Index-Buffer. False, wenn nicht
	};
	struct keepDesc {
		int pointCount;
		glm::vec3 *vertices;
		int *indices;
	};

	//RendertargetManager
	const static int RENDERTARGET0 = -2;
	const static int INVALID_REQUEST = -3;
}