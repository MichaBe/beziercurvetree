#pragma once

#include <windows.h>
#include <gl\glew.h>
#include <gl\wglew.h>
#include <fstream>
#include <string>
#include <vector>
#include <functional>
#include <map>
#include <GLFW\glfw3.h>

class mbc_ContextManager
{
	friend void callBack(GLFWwindow* window, int key, int scancode, int action, int mods);

public:
	mbc_ContextManager(std::pair<int,int> size, bool fullscreen, std::fstream* log, bool logQ, std::string title, int oglMAJOR = 0, int oglMINOR = 0, bool mode = false);
	~mbc_ContextManager(void);

	bool getRunning();
	bool getInFullscreen();
	void SwapTheBuffers();
	void workMessages();
	void workDebugCallBacks();
	GLFWwindow* getWindow();

	void connectKey(int glfwKeyUP, int glfwKeyDOWN, float* variable, float step);
	void connectKey(int glfwKeyUP, int glfwKeyDOWN, double* variable, double step);
	void connectKey(int glfwKeyUP, int glfwKeyDOWN, int* variable, int step);
	void connectKey(int glfwKey, bool* variable);
	void connectKey(int glfwKey, std::function<void(void)> function);

	const static bool DEBUG = true;
	const static bool PROD = false;
	struct msg {
		int key, scancode, action;
	};
	
private:
	bool isRunning;
	bool inFullscreen;
	bool isDebug;

	const static enum KeyConnected{MB_FLOAT, MB_DOUBLE, MB_INT, MB_BOOL, MB_FUNCTION, MB_NONE};
	std::map<int, KeyConnected> connections;
	std::map<int, std::pair<float*, float>> flt_con;
	std::map<int, std::pair<double*, double>> dbl_con;
	std::map<int, std::pair<int*, int>> int_con;
	std::map<int, bool*> boo_con;
	std::map <int, std::function<void(void)>> fnc_con;

	GLFWwindow* myWindow;	
};

