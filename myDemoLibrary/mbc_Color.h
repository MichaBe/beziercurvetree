#pragma once

#include <array>
#include <functional>
#include <algorithm>
#include "mbc_MathManager.h"
#include "mbc.h"

class mbc_Color {
public:
	mbc_Color();
	mbc_Color(int mode, float v1, float v2, float v3);
	~mbc_Color();

	float* data();
	float h();
	float s();
	float v();
	float a();
	void h(float h);
	void s(float s);
	void v(float v);
	void a(float a);
	void interpolate(int position, float in1, float in2, float curIn, std::function<float(float,float,float)> interpolater);

private:
	void calcOutput();//Berechnet RGB-values aus HSV-given
	void rgbTOhsv();
	bool isCorrect;
	std::array<float, 4> values;//Indices: R = 0, G = 1, B = 2, A = 3
	std::array<float, 3> givenV;//Indices: H = 0, S = 1, V = 2
	
};

