#pragma once

#include <rapidxml.hpp>
#include <string>
#include <GL\glew.h>
#include <GL\wglew.h>
#include <fstream>
#include <vector>
#include <map>

class mbc_ShaderManager
{
public:
	mbc_ShaderManager(rapidxml::xml_node<>* shaderManager, std::string projectPath, std::fstream* log, bool logQ);
	~mbc_ShaderManager(void);

	void useProgram(std::string name);
	GLuint getHandle(std::string name);
	void recompile();

private:
	bool mbc_LoadShaderFile(const char* szFile, GLuint shader);
	void loadMBCloader();

	const static int VSHD = 2;	//Vertex-Shader
	const static int FSHD = 3;	//Fragment-Shader
	const static int GSHD = 5;	//Geometry-Shader
	//const static int CSHD = 7;	//Compute-Shader

	std::map<std::string,GLuint> Programs;
	std::map<std::string, int> shaderCombinations;
	std::map<std::string, std::vector<std::string>> shdAttributes;
	std::map<std::string, std::vector<const char*>> shdFeedbacks;
	std::map<std::string, bool> FeedbackStorage;
	std::string path;
};

