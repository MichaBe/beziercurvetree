#pragma once

#include <string>
#include <fstream>
#include <rapidxml.hpp>
#include <functional>
#include <vector>
#include <map>

#include "mbc_Menue.h"
#include "mbc_ShaderManager.h"
#include "mbc_SyncManager.h"
#include "mbc_ContextManager.h"
#include "mbc_ObjectManager.h"
#include "mbc_SoundtrackManager.h"
#include "mbc_TextureManager.h"
#include "mbc_CameraManager.h"
#include "mbc_RendertargetManager.h"
#include "mbc.h"

class mbc_demo
{
public:
	mbc_demo(std::string resPath, unsigned int loadMask = 0, std::string manifestS = "");
	~mbc_demo(void);

	void registerFunction(std::string sceneName, std::function<void(double)> sceneFunction, int type);
	void initScenes();
	void playDemo();
	int sceneTime();
	std::pair<int,int> getWindowSize();

	mbc_ShaderManager* shaderManager;
	mbc_SyncManager* syncManager;
	mbc_ObjectManager* objectManager;
	mbc_ContextManager* contextManager;
	mbc_TextureManager* textureManager;
	mbc_SoundtrackManager* soundManager;
	mbc_CameraManager* cameraManager;
	mbc_RendertargetManager* rendertargetManager;

	bool runs;
	
private:
	mbc_Menue* menue;
	std::string ressourcePath;
	std::string demoName;
	std::pair<int,int> windowSize;
	bool fullScreen;

	bool loadedManagers[6];

	int sceneCount;

	int sceneStartTime;
	int beginTime;
	int currentTime;

	std::map<std::string,int> sceneHandles;
	std::map<int,int> sceneDuration;
	std::map<int,std::function<void(double)>> renderFuntion;
	std::map<int,std::function<void(double)>> calcFunction;
	std::map<int,std::function<void(double)>> initFunction;
	std::map<int,std::function<void(double)>> preFunction;
	std::map<int,std::function<void(double)>> cleanFunction;
};

