#pragma once

#include <math.h>
#include "mbc.h"

static class mbc_MathManager
{
public:

	static float map(float output1, float output2, float curInput);//Takes value from [0,1], maps to [output1,output2]

	static float sinStep(float input1, float input2, float curInput);
	static float linStep(float input1, float input2, float curInput);

	static float alt_sinStep(float input1, float input2, float curInput);
	static float alt_linStep(float input1, float input2, float curInput);


	static float sinHardBegStep(float input1, float input2, float curInput);
	static float sinHardEndStep(float input1, float input2, float curInput);

	static float alt_sinHardStep(float input1, float input2, float curInput);
	static float alt_sinHardMiddleStep(float input1, float input2, float curInput);
};