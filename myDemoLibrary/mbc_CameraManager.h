#pragma once

#include <rapidxml.hpp>
#include <string>
#include <fstream>
#include <map>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include "mbc_MathManager.h"
#include "mbc.h"

class mbc_CameraManager
{
public:
	mbc_CameraManager(rapidxml::xml_node<>* cameraManager, std::fstream* log, bool logQ, int completeDuration, float ratio);
	~mbc_CameraManager(void);
	
	glm::mat4 vpMatrix;
	glm::mat4 rotOnlyM;
	void calcFrame(int TimeMS);
	void changeProjection(float Fov, float Ratio, float Near, float Far);

private:
	float getVAL(float t1, float t2, float v1, float v2, float curT, int interpolation);
	float degToRad(float deg);

	//Aufb.: Zeit-----------Wert---Interpolation
	std::map<int, std::pair<float, int>> ROT_X;
	std::map<int, std::pair<float, int>> ROT_Y;
	std::map<int, std::pair<float, int>> ROT_Z;
	
	std::map<int, std::pair<float, int>> TRA_X;
	std::map<int, std::pair<float, int>> TRA_Y;
	std::map<int, std::pair<float, int>> TRA_Z;

	float mfov;
	float mnear;
	float mfar;
	float mcurRatio;

	glm::mat4 view;
	glm::mat4 projection;
};