#pragma once

#include <rapidxml.hpp>
#include <Windows.h>
#include <string>
#include <iostream>
#include <fstream>
#include <fmod.hpp>
#include <map>

class mbc_SoundtrackManager
{
public:
	mbc_SoundtrackManager(rapidxml::xml_node<>* soundtrackManager, std::string projectPath, std::fstream* log, bool logQ);
	~mbc_SoundtrackManager(void);

	void playSoundtrack(std::string name);
	void stopSoundtrack(std::string name);
	float getSoundTime(std::string name);
	bool mainDoesExist();
	void update();
private:
	std::map<std::string,FMOD::Sound*> Soundtracks;
	std::map<std::string,FMOD::Channel*> Soundchannels;
	std::map<std::string,float> Soundstarttimes;		//_main f�r mainSoundtrack

	FMOD::System* Soundsystem;
	FMOD::ChannelGroup* Soundchannelgroup;
	std::string path;
};