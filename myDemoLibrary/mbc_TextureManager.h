#pragma once

#include <string>
#include <fstream>
#include <iterator>
#include <map>
#include <vector>
#include <GL\glew.h>
#include <GL\wglew.h>
#include <rapidxml.hpp>

#include <iostream>

class mbc_TextureManager
{
public:
	mbc_TextureManager(rapidxml::xml_node<>* textureManager, std::string projectPath, std::fstream* log, bool logQ);
	~mbc_TextureManager(void);

	int bindToSpace(std::string name);
	GLuint getID(std::string name);
	std::pair<int, int> getSize(std::string name);

private:
	bool loadFromFile(int channels, int width, int height, std::string path, unsigned char *bits);
	std::string path;
	int textureCount;
	bool texSpaceFull;
	std::map<std::string,GLuint> Textures;
	std::map<std::string,GLenum> TextureSpace;
	std::map<std::string, std::pair<int, int>> TextureSizes;
};